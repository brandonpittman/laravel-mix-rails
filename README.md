# laravel-mix-rails

Just run `npx larave-mix-rails`. The bin script will take over, copy the package.json and webpack.mix.js files into your project and then run `npm install` and `npm run dev` to make sure everything gets taken care of. After that, just add `mix('public/js/app.js')` and `mix(public/css/app.css)` into your application layout template, and you should be all set!
