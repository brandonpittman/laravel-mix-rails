let mix = require('laravel-mix')
const fs = require('fs')
const path = require('path')

class Rails {
  boot() {
    const helperSource = path.resolve(__dirname, 'lib/laravel_mix_helper.rb')

    const helperTarget = path.resolve(
      __dirname,
      '../../app/helpers/laravel_mix_helper.rb'
    )

    const resourcesSource = path.resolve(
      __dirname,
      'resources'
    )

    const resourcesTarget = path.resolve(
      __dirname,
      '../../resources'
    )

    if (!fs.existsSync(helperTarget)) {
      mix.copy(helperSource, helperTarget)
    }

    if (!fs.existsSync(resourcesTarget))) {
      mix.copyDirectory(resourcesSource, resourcesTarget)
    }

  }
}

mix.extend('rails', new Rails())
